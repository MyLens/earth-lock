import React from 'react';

import Routes from './Routes'; 
import './css/styles.css'
import './css/hero.css'

const App: React.FC = () => {
 
  return (
    <div className="App">
      <Routes/>
    </div>
  );
}

export default App;
