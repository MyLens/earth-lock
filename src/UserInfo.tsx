import React from 'react';
import LensAPI from '@mylens/lens-api';
import SharedLensTable from './components/SharedLensTable';
import { LensRequestResult, LensData } from '@mylens/lens-api';
import NavBar from './components/NavBar'

interface Props {
  location: any,
  history: any
}

interface State {
  api: any,
  loading: boolean,
  data: LensData
}

class UserInfo extends React.Component<Props, State>{

  constructor(props) {
    super(props);
    const clientId = '64b93fca-21de-4c9d-9b0a-48bd605f8a36';
    const requestUrl = (process.env.REACT_APP_REQUEST_URL || "http://vault.mylens.io") + "/lens-fulfillment"; 
    this.state = {
      api: new LensAPI({
        clientId: clientId,
        requestUrl: requestUrl
      }),
      loading: false,
      data: null
    }
  }

  componentDidMount() {
    this.setState({ loading: true })
    try {
      this.state.api.endLensRequest().then((requestResults: LensRequestResult) => {
        try {
          this.state.api.resolveLens(requestResults.lens).then(x => {
            console.log('resolved lens = ', x);
            this.setState({
              data: x.get(),
              loading: false
            })
          }).catch(e => {
            console.error('Error resolving the Lens!', e)
            this.setState({loading: false})
          })
        } catch (e) {
          console.error('An error occurred attempting to grab params, maybe missing?: ', e)
          this.setState({loading: false})
        }
      });
    } catch(e) {
      console.error('Error resolving the Lens!', e)
      this.setState({loading: false})
    }
  }

  render() {
    return (
      <div>
        <section className="hero is-info">
          <div className="hero-head">
          <NavBar/>
          </div>
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className='title'>EarthLock's Subscription</h1>
            </div>
          </div>
        </section>
        <section className="section">
          {
            this.state.loading ? (
              <div className='box has-text-centered'>
              <span className="subtitle">Please wait while we load your lens...</span>
              <progress className="progress" max="100">30%</progress>
              </div>
            ) : (
                <SharedLensTable lensData={this.state.data} />
              )
          }

        </section>
      </div >
    );
  }
}

export default UserInfo;
