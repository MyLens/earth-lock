import React from 'react';
import LensAPI, { LensRequest } from '@mylens/lens-api';

import './LensButton.css'

interface Props  {
    text: string,
    clientId: string,
    requestUrl: string,
    request: LensRequest
}

class LensButton extends React.Component<Props, {}> {

    requestLens() {
        
        const api = new LensAPI({
            clientId: this.props.clientId,
            requestUrl: this.props.requestUrl
        });

        api.beginLensRequest(this.props.request);
    }

    render() {
        const { text, clientId, requestUrl, request, ...others } = this.props;
        return (
            <button
                className="LensButton"
                onClick={this.requestLens.bind(this)}
                {...others}
            >
                <span className="LensButton_sleeve">
                    {this.props.text}
                </span>
            </button>
        )
    }  
}

export default LensButton;
