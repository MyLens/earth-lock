import React from 'react';
import logo from '../logo.png';

interface Props {

}

interface State {
    active: boolean,
    loggedIn: boolean,
    user: string
}

class NavBar extends React.Component<Props, State>{

  constructor(props) {
    super(props);
    let user = localStorage.getItem("user");
    this.state = {
        active: false,
        loggedIn: user ? true : false,
        user: user ? user : null
    }
    console.log(this.state)
    this.hamburgerClicked = this.hamburgerClicked.bind(this);
  }

  handleChange(event) {
      console.log(event)
      this.setState({
          user: event.target.value
      })
  }

  onLogin() {
      localStorage.setItem("user", this.state.user)
    this.setState({
        loggedIn: true
    });
  }

  onLogout() {
    localStorage.removeItem("user")

    this.setState({
        loggedIn: false,
        user: ""
    });
  }

  hamburgerClicked() {
    this.setState({ active: !this.state.active })
  }

  render() {
    return (
        <div>
            <nav className="navbar">
                <div className="container">
                    <div className="navbar-brand">
                        <a className="navbar-item" href="/">
                            <img src={logo} alt="Logo"/>
                        </a>

                        <span className={"navbar-burger burger" + (this.state.active? "is-active" : "")}  aria-label="menu" aria-expanded="false" data-target="navbarMenu" onClick={this.hamburgerClicked}>
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                    </div>
                    <div id="navbarMenu" className={"navbar-menu " + (this.state.active? "is-active" : "")} >
                        <div className="navbar-end">
                            <div className="tabs is-right">
                                <ul>
                                    <li className="is-active"><a href="/">Home</a></li>
                                    <li><a href="">Features</a></li>
                                    <li><a href="">FAQ</a></li>
                                </ul>
                                <span className="navbar-item">
                                    { !this.state.loggedIn ? (
                                        <div>
                                            <input className="login-username" placeholder="Username" onChange={this.handleChange.bind(this)}></input>
                                            <input className="login-password" placeholder="Password" type="password"></input>
                                        </div>
                                    ) : (
                                        <div>
                                            Welcome {this.state.user}
                                        </div>
                                    )}
                                </span>
                                <span className="navbar-item">
                                    {
                                        this.state.loggedIn
                                        ?
                                        <a className="button is-white is-outlined">
                                            <span title="Login" onClick={this.onLogout.bind(this)}>Logout</span>
                                        </a>
                                        : 
                                        <a className="button is-white is-outlined">
                                            
                                            <span title="Logout" onClick={this.onLogin.bind(this)}>Login</span>
                                        </a>
                                    }
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    );
  }
}

export default NavBar;
