import React from 'react';
import { LensData } from '@mylens/lens-api'
import './SharedLensTable.css'
interface Props {
    lensData?: LensData,
}

const SharedLensTable: React.FC<Props> = (props: Props) => {

    console.log("LENSDATA", props.lensData);
    if (props.lensData == null || props.lensData.length == 0) {
        return (
            <div className="container box has-text-centered">
                <span className="title has-text-danger">No Data Was Received</span>
            </div>
        )
    } else {
        return (
            <div className="container box has-text-centered">
                    <span className="subtitle">
                        You gave <i>EarthLock</i> a subscription to your data below
                    </span>
        
                <div className="level">
                    <div className="level-item">
                        <div className="table-container">
                            <table className="table is-bordered is-striped">
                                <thead>
                                    <tr>

                                        <th><strong>Name</strong></th>

                                        <th><strong>Value</strong></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {props.lensData ? props.lensData.toArray().map(x => {
                                        return (
                                            <tr key={x.id}>

                                                <td>{x.name}</td>

                                                <td>{x.value}</td>
                                            </tr>
                                        )
                                    }) : null}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        )
    }

}

export default SharedLensTable;
