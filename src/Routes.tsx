import { BrowserRouter as Router, Route } from "react-router-dom";
import Home from './Home'; 
import UserInfo from './UserInfo';
import React from 'react';


const Routes: React.FC = () => {
    return (
    <Router>
        <Route path="/" exact component={Home} />
        <Route path="/user-info" exact component={UserInfo}/>
    </Router>
)}

export default Routes;
