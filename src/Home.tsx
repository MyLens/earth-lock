import React from 'react';
import './App.css';
import LensAPI, { LensRequestTargetType, LensRequest } from '@mylens/lens-api';
import NavBar from './components/NavBar'
import LensButton from './components/LensButton';
import '../node_modules/@fortawesome/fontawesome-free/css/all.min.css'

const Home: React.FC = (props: any) => {
    let req : LensRequest = { 
        redirectUri: (process.env.REACT_APP_REDIRECT_URL || "https://earthlock.mylens.io") + '/user-info',
        target: LensRequestTargetType.General,
        data: [{
            name: "firstName",
            type: "string",
            label: "First Name",
            validators: ['required']
        },
        {
            name: "lastName",
            type: "string",
            label: "Last Name",
            validators: ['required']
        },
        {
            name: "emailAddress",
            type: "string/email-address",
            label: "Email Address",
            validators: ['required']
        },
        {
            name: "phoneNumber",
            type: "string/phone-number",
            label: "Phone Number"
        },
        {
            name: "country",
            type: "string",
            label: "Country of Residence",
            display: 'select',
            options: ['United States', 'Canada', 'United Kingdom'],
            validators: ['options']
        },
        {
            name: "bikeToWorkPerWeek",
            type: "number/integer",
            min: 0,
            max: 7,
            label: "How many times a week do you bike to work?"
        },
        {
            name: "flightsPerYear",
            type: "number/integer",
            min: 0,
            label: "How many flights do you take per year?"
        },
        {
            name: "typeOfVehicle",
            type: "string",
            label: "What type of vehicle do you own?"
        },
        {
            name: "diet",
            type: "string",
            label: "Describe your typical diet",
            display: 'select',
            options: ["Omnivore","Vegetarian", "Vegan", "Carnivore"],
            validators: ['options']
        }]
    } 

    console.log('Requested the following data: ', req)

    const clientId = '64b93fca-21de-4c9d-9b0a-48bd605f8a36'
    const requestUrl = (process.env.REACT_APP_REQUEST_URL || "http://vault.mylens.io") + "/lens-fulfillment"; 

  return (
    <div>
       <section className="hero is-info is-medium is-bold">
        <div className="hero-head">
           <NavBar/>
        </div>
        <div className="hero-body">
            <div className="container has-text-centered">
                <h1 className="title">
                EarthLock
                </h1>
                <h2 className="subtitle">
                    Join the millions of users who are making decisions that benefit themselves and the earth. 
                    We provide a service that rewards individuals for making earth conscious decisions by subscribing 
                    to data you generate daily with your permission. 
                </h2>
            </div>
        </div>
    </section>

 <div className="box cta">
    <div className="has-text-centered">
        <h2 className="subtitle">We want to subscribe to your data to provide you better services and help solve world's problems!</h2>
        <div className="cta">
            <div>
                <form>
                    <div className="field">
                        <label>First Name</label>
                        <input type="text"/>
                    </div>

                    <div className="field">
                        <label>Family Name</label>
                        <input type="text"/>
                    </div>

                    <div className="field">
                        <label>Email Address</label>
                        <input type="email"/>
                    </div>

                    <div className="field">
                        <label>Phone Number</label>
                        <input type="tel"/>
                    </div>

                    <div className="field">
                        <label>Country</label>
                        <input type="text"/>
                    </div>

                    <div className="field">
                        <label>Bike to Work per Week</label>
                        <input type="number"/>
                    </div>

                    <div className="field">
                        <label>Flights Per Year</label>
                        <input type="number"/>
                    </div>

                    <div className="field">
                        <label>Type of Vehicle</label>
                        <input type="text"/>
                    </div>

                    <div className="field">
                        <label>Diet</label>
                        <input type="text"/>
                    </div>
                </form>
                - OR -
                <LensButton 
                    request={req} 
                    clientId={clientId}
                    requestUrl={requestUrl}
                    text="Share Lens"
                    />
            </div>
            <p className="has-text-centered">
                Use Lens to share data with us, and get a jump start on helping
            </p>
        </div>
    </div>
</div>

<section className="container">
    <div className="columns features has-text-centered">
      
        <div className="column is-4" >
            <div className="card is-shady" data-aos="zoom-in">
                <div className="card-image has-text-centered">
                    <i className="fas fa-bicycle"></i>
                </div>
                <div className="card-content" >
                    <div className="content">
                        <h4>Use alternative transportation</h4>
                        <p>Track your mileage on your bicycle or your steps instead of driving a car</p>
                        <p><a href="#">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div className="column is-4">
            <div className="card is-shady" data-aos="zoom-in">
                <div className="card-image has-text-centered">
                  <i className="fab fa-bitcoin"></i>
                </div>
                <div className="card-content">
                    <div className="content">
                        <h4>Receive Money</h4>
                        <p>Be rewarded for your efforts in cryptocurrency. When you allow us to subscribe to your data, we pay you 
                          for that subscription so we can keep our models up to date. The more current your data is, the more you earn! 
                        </p>
                        <p><a href="#">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
        <div className="column is-4">
            <div className="card is-shady" data-aos="zoom-in">
                <div className="card-image has-text-centered">
                <i className="fas fa-globe-africa"></i>
                </div>
                <div className="card-content">
                    <div className="content">
                        <h4>Make a Positive Impact</h4>
                        <p>Your efforts could have major impacts for your community and those around the world!</p>
                        <p><a href="#">Learn more</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<footer className="footer">
    <div className="container">
        <div className="columns">
            <div className="column is-3 is-offset-2">
                <h2><strong>Category</strong></h2>
                <ul>
                    <li><a href="#">Lorem ipsum dolor sit amet</a></li>
                    <li><a href="#">Vestibulum errato isse</a></li>

                </ul>
            </div>
            <div className="column is-3">
                <h2><strong>Category</strong></h2>
                <ul>
                    <li><a href="#">Labore et dolore magna aliqua</a></li>
                    <li><a href="#">Kanban airis sum eschelor</a></li>
                </ul>
            </div>
            <div className="column is-4">
                <h2><strong>Category</strong></h2>
                <ul>
                    <li><a href="#">Objects in space</a></li>
 
                </ul>
            </div>
        </div>

    </div>
  
</footer> 
  </div>
  );
}

export default Home;
